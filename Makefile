
################################################################################
# Shortcuts
################################################################################

_TEMP_FILE := $(shell mktemp)
GITLAB_TOKEN ?= $(shell vault kv get -field=GITLAB_PASSWORD secret/qm64/providers)

recover_root:
	# Generating a new root token, please revoke it when you are done.
	# Later run `vault operator generate-root` for each sealing key"
	-vault operator generate-root -init 
	# Once done, please log in and use `make token_admin` to generate a proper
	# token. This will increase security level. Remember to revoke.
	# 
	# To approve, ask the unsealing key holders to run 
	# `vault operator generate-root` and then you can run:
	# `vault operator generate-root -decode=$OUTPUT +XDwcOC8 -otp=$OTP_KEY`
.PHONY: recover_root

start_unseal_rekey:
	# Generate a new set of keys. This will invalidate the previous unseal keys
	vault operator rekey -init \
		-key-shares=3 -key-threshold=2
	# Now you can run `vault operator rekey`
.PHONY: start_rekey

seal:
	vault operator seal
.PHONY: seal

rotate:
	vault operator rotate
	@sleep 3
.PHONY: rotate 

panic: 
	-$(make) rotate 
	$(make) seal
.PHONY: panic

token_pipeline:
	# Create a pipeline token capable of reading atlas secrets, that has 1w as life
	# time and can be renewed for a max of 1 year. It is orphan so that when
	# revoking the root token this will not be revoked too.
	#
	vault token create \
		-orphan -display-name="pipeline" \
		-policy=pipeline \
		-period=216h -ttl=216h -explicit-max-ttl=8766h -field=token > ${_TEMP_FILE}
	#
	# Update token for Qm64
	curl --request PUT --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
		"https://gitlab.com/api/v4/groups/6662294/variables/VAULT_TOKEN" \
		--form "value=`cat ${_TEMP_FILE}`"
	#
	# Update token for Siderus
	curl --request PUT --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
		"https://gitlab.com/api/v4/groups/1744282/variables/VAULT_TOKEN" \
		--form "value=`cat ${_TEMP_FILE}`"

.PHONY: token_pipeline

token_admin:
	# Add admin token capable of running as root. This will help revoking root
	# token to increase security. Use this token with caution. It will expire
	# in 7 days and can be renewed for a max of 1 month. This token should
	# be used just for set up and monitoring during the next month.
	#
	vault token create -orphan -policy=root -period=168h \
		-explicit-max-ttl=744h -ttl=168h
	@echo "--- Press Enter when ready to revoke the root token ---"
	@read
	#
	# Since we have a new temporary root token, we can revoke the previous one
	# Please login with the new token once done.
	#
	vault token revoke -self
.PHONY: token_admin

.DEFAULT_GOAL := token_quick_admin
token_quick_admin:
	# Add admin token capable of running as root.
	# This is a short living key that can't survive more than 3h (default 5m).
	# Use this for things like browser or quick tests.
	vault token create -policy=root -period=15m -explicit-max-ttl=3h -ttl=5m
.PHONY: token_quick_admin

ssh_client:
	# Gain access to SSH by signing your own SSH id_rsa key.
	vault write -field=signed_key ssh/sign/admin public_key=@${HOME}/.ssh/id_rsa.pub > ${HOME}/.ssh/id_rsa-cert.pub
.PHONY: ssh_client
