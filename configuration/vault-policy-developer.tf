resource "vault_policy" "developer" {
  name = "developer"

  policy = <<EOT
# AWS Access as reader
path "aws/creds/reader" {
  capabilities = ["read"]
}
path "sys/leases/revoke/aws/creds/*" {
  capabilities = ["create", "update", "read"]
}

# Packer requirements
path "secret/data/*" {
  capabilities = ["create", "update", "read", "list"]
}

# Read/Write/Update secrets
path "secret/*" {
  capabilities = ["list", "read", "update", "create"]
}

path "secret/data/*" {
  capabilities = ["create","update","read", "list"]
}

# Ability to rotate keys
path "postgres/rotate-root/*"{
  capabilities = ["create", "update", "read", "list"]
}

# Allow the Pipeline to seal vault in case of emergencies
path "sys/seal"
{
  capabilities = ["read", "update", "sudo"]
}

# Allow the pipeline to rotate the underlying keys (done every 6 months)
path "sys/rotate"
{
  capabilities = ["update", "sudo"]
}

# Ability to get SSH CA
path "ssh/config/ca"
{
  capabilities = ["read", "list"]
}
EOT
}