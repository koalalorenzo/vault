resource "vault_github_auth_backend" "qm64" {
  organization = "qm64-tech"
}

resource "vault_github_user" "koalalorenzo" {
  backend  = vault_github_auth_backend.qm64.id
  user     = "koalalorenzo"
  policies = [
    "default",
    vault_policy.read_only.name,
    vault_policy.developer.name,
  ]
}