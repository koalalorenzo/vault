resource "vault_policy" "read_only" {
  name = "read-only"

  policy = <<EOT
# Read secrets
path "secret/*" {
  capabilities = ["list"]
}

path "secret/data/*" {
  capabilities = ["read", "list"]
}

path "transit/encrypt/*" {
  capabilities = ["create", "update", "read"]
}

path "transit/decrypt/*" {
  capabilities = ["create", "update", "read"]
}
EOT
}